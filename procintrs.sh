#!/bin/bash
#######################################################################
# AMD_PROLOG_BEGIN_TAG                                                 #
#                                                                      #
# Licensed Materials - Property of AMD                                 #
#                                                                      #
# (C) COPYRIGHT Advanced Micro Devices, Inc. 2018                      #
# All Rights Reserved                                                  #
#                                                                      #
# AUTHOR:  Ben Gibbs                                                   #
# EMAIL:   Ben.Gibbs@amd.com                                           #
# VERSION: 1.0                                                         #
#                                                                      #
# AMD_PROLOG_END_TAG                                                   #
########################################################################
PROGCLI=$0
PROGNAME=${0##*/}
PROGVERSION=1.0

# Partial format of /proc/interrupts showing just the first 8 CPUs.
#         CPU0       CPU1       CPU2       CPU3       CPU4       CPU5       CPU6       CPU7
# 0:        36          0          0          0          0          0          0          0    IO-APIC   2-edge      timer
# 8:         0          1          0          0          0          0          0          0    IO-APIC   8-edge      rtc0
# 9:         0          0          0          0          0          0          0          0    IO-APIC   9-fasteoi   acpi
#16:         0          0          0          0          0          0          0          0    IO-APIC  16-fasteoi   uhci_hcd:usb3, pata_marvell

outFILE=${1:-/dev/stdin}
if [ "${outFILE}" = "" ]
then
    echo "Syntax: ${PROGNAME} <file that contains /proc/interrupts output>"
    exit 2
fi

if [ ! -f ${outFILE} ] && [ ! -L ${outFILE} ]
then
    echo "${PROGNAME}: The file ${outFILE} does not exist...Exiting."
    exit 2
fi

# Find out how many CPUs there are
declare -a intr_array
readarray intr_array < ${outFILE}

numCPUS=$(echo ${intr_array[0]} | grep --max-count=1 "CPU0" | wc -w)

if [ ${numCPUS} -eq 0 ]
then
    echo "${PROGNAME}: This is not a valid /proc/interrupts data."
    echo "              First row should be the CPU headers."
    exit 1
fi

descSTART=$((numCPUS + 2))
intrSECTIONS=$((numCPUS / 8))
oddSECTIONS=$((numCPUS % 8))
numINTRS=${#intr_array[@]}
(( numINTRS -= 1 ))             # Adjust for header, e.g., CPU0 CPU1....

let isecs=0

if [ ${oddSECTIONS} -gt 0 ]
then
    (( intrSECTIONS += 1 ))     # To handle the odd number of CPUs
fi
let startCOL=1
let endCOL=8

while (( ${intrSECTIONS} > isecs ))
do
    cpuHDR=$(echo ${intr_array[0]} | sed 's/\s\+/ /g' | cut -d' ' -f${startCOL}-${endCOL})
    numHDR=$(echo $cpuHDR | wc -w)
    if [ "${numHDR}" = "8" ]
    then
        printf "\n%6s %10s %10s %10s %10s %10s %10s %10s %10s %s\n" "IRQ" ${cpuHDR} " Description"
    else
        printf "\n%6s " "IRQ"
        for cpu in ${cpuHDR}
        do
            printf "%10s " ${cpu}
        done
        echo " Description"
    fi

    let nrows=1
    (( startCOL += 1 ))
    while (( ${numINTRS} > nrows ))
    do
        (( iendCOL = endCOL + 1 ))              # Adjust for IRQ column
        iDATA=$(echo ${intr_array[${nrows}]} | sed 's/\s\+/ /g' | cut -d' ' -f1,${startCOL}-${iendCOL})
        DESC=$(echo ${intr_array[${nrows}]} | sed 's/\s\+/ /g' | cut -d' ' -f${descSTART}-)
        if [ "${numHDR}" = "8" ]
        then
            printf "%6s %10s %10s %10s %10s %10s %10s %10s %10s " ${iDATA}
        else
            pIRQ=$(echo ${iDATA} | cut -d' ' -f1)
            pVALS=$(echo ${iDATA} | cut -d' ' -f2-)
            printf "%6s "  ${pIRQ}
            if [ $(echo ${pVALS} | wc -w) -gt 1 ]
            then
                lim=0
                for val in ${pVALS}
                do
                    if [ "${lim}" -lt "${numHDR}" ]
                    then
                        printf "%10s " ${val}
                    fi
                    (( lim = lim + 1 ))
                done
            fi
        fi
        echo " ${DESC}"
        (( nrows += 1 ))
    done
    (( isecs += 1 ))
    (( startCOL = endCOL + 1 ))
    (( endCOL = startCOL + 7 ))
done
exit 0
